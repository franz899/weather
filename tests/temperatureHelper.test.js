const temperatureHelper = require('../helpers/temperatureHelper');

describe('temperatureHelper', () => {
  describe('convertFahrenheitToCelsius', () => {
    it('can convert fahrenheit to celsius', () => {
      const convertedTemperature = temperatureHelper.convertFahrenheitToCelsius(86);
      expect(convertedTemperature).toBe(30);
    });

    it('can convert kelvin to celsius', () => {
      
    });
  });
});
