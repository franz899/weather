const urlHelper = require('../helpers/urlHelper');

describe('urlHelper', () => {
  describe('generateUrl', () => {
    const baseUrl = 'https://api.openweathermap.org/data/2.5/forecast?';
    const query = {
      q: 'London,uk',
      units: 'metric',
      appid: 'qwerty',
    };

    it('converts the url', () => {
      const url = urlHelper.generateUrl(baseUrl, query);
      expect(url).toBe('https://api.openweathermap.org/data/2.5/forecast?q=London,uk&units=metric&appid=qwerty');
    });
  });
});


