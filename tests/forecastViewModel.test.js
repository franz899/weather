const {
  fromJSTimeStamp,
  getDateKey,
  getTimeKey,
} = require('../helpers/dateTimeHelper');

const {
  toCompass,
  forecast,
  groupDays,
  eighth,
} = require('../viewmodels/forecastViewModel');
const data = require('./forecastViewModel.data');

describe('forecastViewModel', () => {
  describe('toCompass', () => {
    it('converts degrees in the right direction', () => {
      const degrees = [0, 45, 90, 135, 180, 225, 270, 315];
      const directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
      degrees.forEach((deg, i) => {
        expect(toCompass(deg)).toBe(directions[i]);
      });
    });
  });

  describe('groupDays', () => {
    it('should group the days', () => {
      const list = groupDays(data.list);
      Object.keys(list).forEach(key => {
        expect(list[key]).toHaveLength(8);
      });
    });
  });

  describe('eighth', () => {
    it('can convert a forecasts eight of a day', () => {
      const day = eighth(data.list[0]);
      expect(day).toMatchObject({
        time: '00:00',
        temp: 7.17,
        humidity: 83,
        pressure: 1038.49,
        description: 'light rain',
        wind: {
          speed: 4.01,
          direction: 'WSW',
        },
        rain: 0.0275,
        snow: null,
      });
    });
  });

  describe('forecast', () => {
    it('converts a forecast', () => {
      const vm = forecast(data);
      expect(vm.city).toMatchObject({
        name: "London",
        coord: {
          lat: 51.5085,
          lon: -0.1258
        },
        country: "GB"
      });
    });
  });
});