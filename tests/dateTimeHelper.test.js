const {
  fromJSTimeStamp,
  getDateKey,
  getTimeKey,
} = require('../helpers/dateTimeHelper');

describe('dateTimeHelper', () => {
  describe('fromMillis', () => {
    it("converts the date from Javascript DateTime", () => {
      const date = fromJSTimeStamp(1512507600);
      expect(date).toMatchObject({
        "day": 5,
        "hour": 21,
        "millisecond": 0,
        "minute": 0,
        "month": 12,
        "second": 0,
        "year": 2017
      });
    });
  });

  describe('getDateKey', () => {
    it('should return the right format', () => {
      const date = fromJSTimeStamp(1512507600);
      expect(getDateKey(date)).toBe('05-12-2017');
    });
  });

  describe('getTimeKey', () => {
    it('should return the right format', () => {
      const date = fromJSTimeStamp(1512507600);
      expect(getTimeKey(date)).toBe('21:00');
    });
  });
});