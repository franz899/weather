module.exports = {
	"cod": "200",
	"message": 0.004,
	"cnt": 40,
	"list": [
		{
			"dt": 1512518400,
			"main": {
				"temp": 7.17,
				"temp_min": 6.06,
				"temp_max": 7.17,
				"pressure": 1038.49,
				"sea_level": 1046.36,
				"grnd_level": 1038.49,
				"humidity": 83,
				"temp_kf": 1.11
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 36
			},
			"wind": {
				"speed": 4.01,
				"deg": 239.003
			},
			"rain": {
				"3h": 0.0275
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-06 00:00:00"
		},
		{
			"dt": 1512529200,
			"main": {
				"temp": 5.73,
				"temp_min": 4.99,
				"temp_max": 5.73,
				"pressure": 1037.26,
				"sea_level": 1045.12,
				"grnd_level": 1037.26,
				"humidity": 91,
				"temp_kf": 0.74
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 24
			},
			"wind": {
				"speed": 4.03,
				"deg": 242.001
			},
			"rain": {
				"3h": 0.01
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-06 03:00:00"
		},
		{
			"dt": 1512540000,
			"main": {
				"temp": 6.44,
				"temp_min": 6.07,
				"temp_max": 6.44,
				"pressure": 1035.99,
				"sea_level": 1043.86,
				"grnd_level": 1035.99,
				"humidity": 93,
				"temp_kf": 0.37
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 3.91,
				"deg": 237
			},
			"rain": {
				"3h": 0.085
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-06 06:00:00"
		},
		{
			"dt": 1512550800,
			"main": {
				"temp": 7.31,
				"temp_min": 7.31,
				"temp_max": 7.31,
				"pressure": 1034.66,
				"sea_level": 1042.49,
				"grnd_level": 1034.66,
				"humidity": 90,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 64
			},
			"wind": {
				"speed": 4.42,
				"deg": 228
			},
			"rain": {
				"3h": 0.06
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-06 09:00:00"
		},
		{
			"dt": 1512561600,
			"main": {
				"temp": 9.65,
				"temp_min": 9.65,
				"temp_max": 9.65,
				"pressure": 1032.59,
				"sea_level": 1040.25,
				"grnd_level": 1032.59,
				"humidity": 81,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 76
			},
			"wind": {
				"speed": 6.18,
				"deg": 226.003
			},
			"rain": {
				"3h": 0.045
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-06 12:00:00"
		},
		{
			"dt": 1512572400,
			"main": {
				"temp": 9.6,
				"temp_min": 9.6,
				"temp_max": 9.6,
				"pressure": 1029.92,
				"sea_level": 1037.61,
				"grnd_level": 1029.92,
				"humidity": 80,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 80
			},
			"wind": {
				"speed": 6.7,
				"deg": 222.5
			},
			"rain": {
				"3h": 0.02
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-06 15:00:00"
		},
		{
			"dt": 1512583200,
			"main": {
				"temp": 9.64,
				"temp_min": 9.64,
				"temp_max": 9.64,
				"pressure": 1027.61,
				"sea_level": 1035.21,
				"grnd_level": 1027.61,
				"humidity": 79,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 7.01,
				"deg": 219.001
			},
			"rain": {
				"3h": 0.055
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-06 18:00:00"
		},
		{
			"dt": 1512594000,
			"main": {
				"temp": 10.06,
				"temp_min": 10.06,
				"temp_max": 10.06,
				"pressure": 1024.62,
				"sea_level": 1032.3,
				"grnd_level": 1024.62,
				"humidity": 81,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 7.61,
				"deg": 216.502
			},
			"rain": {
				"3h": 0.16
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-06 21:00:00"
		},
		{
			"dt": 1512604800,
			"main": {
				"temp": 10.49,
				"temp_min": 10.49,
				"temp_max": 10.49,
				"pressure": 1021.25,
				"sea_level": 1028.85,
				"grnd_level": 1021.25,
				"humidity": 79,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 8.27,
				"deg": 216.004
			},
			"rain": {
				"3h": 0.125
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-07 00:00:00"
		},
		{
			"dt": 1512615600,
			"main": {
				"temp": 11.17,
				"temp_min": 11.17,
				"temp_max": 11.17,
				"pressure": 1017.12,
				"sea_level": 1024.75,
				"grnd_level": 1017.12,
				"humidity": 85,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 8.92,
				"deg": 215.503
			},
			"rain": {
				"3h": 0.56
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-07 03:00:00"
		},
		{
			"dt": 1512626400,
			"main": {
				"temp": 12.02,
				"temp_min": 12.02,
				"temp_max": 12.02,
				"pressure": 1013.09,
				"sea_level": 1020.58,
				"grnd_level": 1013.09,
				"humidity": 92,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 9.21,
				"deg": 219.01
			},
			"rain": {
				"3h": 1.01
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-07 06:00:00"
		},
		{
			"dt": 1512637200,
			"main": {
				"temp": 12.61,
				"temp_min": 12.61,
				"temp_max": 12.61,
				"pressure": 1009.46,
				"sea_level": 1016.96,
				"grnd_level": 1009.46,
				"humidity": 95,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 9.09,
				"deg": 220
			},
			"rain": {
				"3h": 1.81
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-07 09:00:00"
		},
		{
			"dt": 1512648000,
			"main": {
				"temp": 12.79,
				"temp_min": 12.79,
				"temp_max": 12.79,
				"pressure": 1006.45,
				"sea_level": 1013.97,
				"grnd_level": 1006.45,
				"humidity": 96,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 501,
					"main": "Rain",
					"description": "moderate rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 8.01,
				"deg": 233.001
			},
			"rain": {
				"3h": 4.95
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-07 12:00:00"
		},
		{
			"dt": 1512658800,
			"main": {
				"temp": 7.43,
				"temp_min": 7.43,
				"temp_max": 7.43,
				"pressure": 1008.59,
				"sea_level": 1016.21,
				"grnd_level": 1008.59,
				"humidity": 96,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 56
			},
			"wind": {
				"speed": 7.06,
				"deg": 286.501
			},
			"rain": {
				"3h": 1.35
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-07 15:00:00"
		},
		{
			"dt": 1512669600,
			"main": {
				"temp": 5.98,
				"temp_min": 5.98,
				"temp_max": 5.98,
				"pressure": 1009.91,
				"sea_level": 1017.52,
				"grnd_level": 1009.91,
				"humidity": 93,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"clouds": {
				"all": 68
			},
			"wind": {
				"speed": 7.58,
				"deg": 275.005
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-07 18:00:00"
		},
		{
			"dt": 1512680400,
			"main": {
				"temp": 5.59,
				"temp_min": 5.59,
				"temp_max": 5.59,
				"pressure": 1011.16,
				"sea_level": 1018.79,
				"grnd_level": 1011.16,
				"humidity": 96,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"clouds": {
				"all": 56
			},
			"wind": {
				"speed": 6.31,
				"deg": 283.503
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-07 21:00:00"
		},
		{
			"dt": 1512691200,
			"main": {
				"temp": 5.5,
				"temp_min": 5.5,
				"temp_max": 5.5,
				"pressure": 1012.29,
				"sea_level": 1019.97,
				"grnd_level": 1012.29,
				"humidity": 98,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 4.45,
				"deg": 288.003
			},
			"rain": {
				"3h": 0.46
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-08 00:00:00"
		},
		{
			"dt": 1512702000,
			"main": {
				"temp": 3.83,
				"temp_min": 3.83,
				"temp_max": 3.83,
				"pressure": 1013.53,
				"sea_level": 1021.24,
				"grnd_level": 1013.53,
				"humidity": 99,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"clouds": {
				"all": 76
			},
			"wind": {
				"speed": 3.86,
				"deg": 310.501
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-08 03:00:00"
		},
		{
			"dt": 1512712800,
			"main": {
				"temp": 3.12,
				"temp_min": 3.12,
				"temp_max": 3.12,
				"pressure": 1014.84,
				"sea_level": 1022.56,
				"grnd_level": 1014.84,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"clouds": {
				"all": 80
			},
			"wind": {
				"speed": 4.98,
				"deg": 302.5
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-08 06:00:00"
		},
		{
			"dt": 1512723600,
			"main": {
				"temp": 3.21,
				"temp_min": 3.21,
				"temp_max": 3.21,
				"pressure": 1016.83,
				"sea_level": 1024.58,
				"grnd_level": 1016.83,
				"humidity": 97,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 56
			},
			"wind": {
				"speed": 5.43,
				"deg": 298.502
			},
			"rain": {
				"3h": 0.02
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-08 09:00:00"
		},
		{
			"dt": 1512734400,
			"main": {
				"temp": 4.13,
				"temp_min": 4.13,
				"temp_max": 4.13,
				"pressure": 1018.26,
				"sea_level": 1026.01,
				"grnd_level": 1018.26,
				"humidity": 89,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02d"
				}
			],
			"clouds": {
				"all": 12
			},
			"wind": {
				"speed": 5.22,
				"deg": 306.006
			},
			"rain": {},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-08 12:00:00"
		},
		{
			"dt": 1512745200,
			"main": {
				"temp": 3.06,
				"temp_min": 3.06,
				"temp_max": 3.06,
				"pressure": 1018.12,
				"sea_level": 1025.97,
				"grnd_level": 1018.12,
				"humidity": 98,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 68
			},
			"wind": {
				"speed": 5.9,
				"deg": 302.001
			},
			"rain": {
				"3h": 1.55
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-08 15:00:00"
		},
		{
			"dt": 1512756000,
			"main": {
				"temp": 2.08,
				"temp_min": 2.08,
				"temp_max": 2.08,
				"pressure": 1019.27,
				"sea_level": 1027.13,
				"grnd_level": 1019.27,
				"humidity": 92,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "02n"
				}
			],
			"clouds": {
				"all": 8
			},
			"wind": {
				"speed": 6.41,
				"deg": 309.001
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-08 18:00:00"
		},
		{
			"dt": 1512766800,
			"main": {
				"temp": 1.59,
				"temp_min": 1.59,
				"temp_max": 1.59,
				"pressure": 1020.4,
				"sea_level": 1028.23,
				"grnd_level": 1020.4,
				"humidity": 93,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 6.55,
				"deg": 308.006
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-08 21:00:00"
		},
		{
			"dt": 1512777600,
			"main": {
				"temp": 0.88,
				"temp_min": 0.88,
				"temp_max": 0.88,
				"pressure": 1021.82,
				"sea_level": 1029.7,
				"grnd_level": 1021.82,
				"humidity": 88,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 6.06,
				"deg": 312.5
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-09 00:00:00"
		},
		{
			"dt": 1512788400,
			"main": {
				"temp": 0.21,
				"temp_min": 0.21,
				"temp_max": 0.21,
				"pressure": 1022.75,
				"sea_level": 1030.61,
				"grnd_level": 1022.75,
				"humidity": 82,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 5.45,
				"deg": 313
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-09 03:00:00"
		},
		{
			"dt": 1512799200,
			"main": {
				"temp": 0.26,
				"temp_min": 0.26,
				"temp_max": 0.26,
				"pressure": 1022.72,
				"sea_level": 1030.65,
				"grnd_level": 1022.72,
				"humidity": 81,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 4.97,
				"deg": 303.5
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-09 06:00:00"
		},
		{
			"dt": 1512810000,
			"main": {
				"temp": 0.84,
				"temp_min": 0.84,
				"temp_max": 0.84,
				"pressure": 1023.4,
				"sea_level": 1031.36,
				"grnd_level": 1023.4,
				"humidity": 83,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 4.76,
				"deg": 302.501
			},
			"rain": {},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-09 09:00:00"
		},
		{
			"dt": 1512820800,
			"main": {
				"temp": 3.88,
				"temp_min": 3.88,
				"temp_max": 3.88,
				"pressure": 1023.25,
				"sea_level": 1031.1,
				"grnd_level": 1023.25,
				"humidity": 81,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 4.78,
				"deg": 295.001
			},
			"rain": {},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-09 12:00:00"
		},
		{
			"dt": 1512831600,
			"main": {
				"temp": 4.2,
				"temp_min": 4.2,
				"temp_max": 4.2,
				"pressure": 1021.89,
				"sea_level": 1029.76,
				"grnd_level": 1021.89,
				"humidity": 79,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02d"
				}
			],
			"clouds": {
				"all": 12
			},
			"wind": {
				"speed": 4.11,
				"deg": 294.003
			},
			"rain": {},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-09 15:00:00"
		},
		{
			"dt": 1512842400,
			"main": {
				"temp": 1.82,
				"temp_min": 1.82,
				"temp_max": 1.82,
				"pressure": 1020.89,
				"sea_level": 1028.69,
				"grnd_level": 1020.89,
				"humidity": 89,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 3.32,
				"deg": 275.002
			},
			"rain": {
				"3h": 0.0099999999999998
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-09 18:00:00"
		},
		{
			"dt": 1512853200,
			"main": {
				"temp": -1.25,
				"temp_min": -1.25,
				"temp_max": -1.25,
				"pressure": 1018.92,
				"sea_level": 1026.85,
				"grnd_level": 1018.92,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 1.86,
				"deg": 245.503
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-09 21:00:00"
		},
		{
			"dt": 1512864000,
			"main": {
				"temp": -3.57,
				"temp_min": -3.57,
				"temp_max": -3.57,
				"pressure": 1015.32,
				"sea_level": 1023.28,
				"grnd_level": 1015.32,
				"humidity": 93,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"clouds": {
				"all": 64
			},
			"wind": {
				"speed": 1.26,
				"deg": 166.501
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-10 00:00:00"
		},
		{
			"dt": 1512874800,
			"main": {
				"temp": -1.15,
				"temp_min": -1.15,
				"temp_max": -1.15,
				"pressure": 1009.35,
				"sea_level": 1017.13,
				"grnd_level": 1009.35,
				"humidity": 92,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"clouds": {
				"all": 88
			},
			"wind": {
				"speed": 2.56,
				"deg": 122.006
			},
			"rain": {},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-10 03:00:00"
		},
		{
			"dt": 1512885600,
			"main": {
				"temp": 2.2,
				"temp_min": 2.2,
				"temp_max": 2.2,
				"pressure": 1000.41,
				"sea_level": 1008.05,
				"grnd_level": 1000.41,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 5.02,
				"deg": 132.502
			},
			"rain": {
				"3h": 2.2
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-10 06:00:00"
		},
		{
			"dt": 1512896400,
			"main": {
				"temp": 2.17,
				"temp_min": 2.17,
				"temp_max": 2.17,
				"pressure": 990.16,
				"sea_level": 997.68,
				"grnd_level": 990.16,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 501,
					"main": "Rain",
					"description": "moderate rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 8.05,
				"deg": 130.5
			},
			"rain": {
				"3h": 7.63
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-10 09:00:00"
		},
		{
			"dt": 1512907200,
			"main": {
				"temp": 3,
				"temp_min": 3,
				"temp_max": 3,
				"pressure": 982.05,
				"sea_level": 989.68,
				"grnd_level": 982.05,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 501,
					"main": "Rain",
					"description": "moderate rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 100
			},
			"wind": {
				"speed": 6.06,
				"deg": 115.504
			},
			"rain": {
				"3h": 7.83
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-10 12:00:00"
		},
		{
			"dt": 1512918000,
			"main": {
				"temp": 5.66,
				"temp_min": 5.66,
				"temp_max": 5.66,
				"pressure": 978.09,
				"sea_level": 985.73,
				"grnd_level": 978.09,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": {
				"all": 100
			},
			"wind": {
				"speed": 0.5,
				"deg": 237.002
			},
			"rain": {
				"3h": 2.39
			},
			"sys": {
				"pod": "d"
			},
			"dt_txt": "2017-12-10 15:00:00"
		},
		{
			"dt": 1512928800,
			"main": {
				"temp": 5.36,
				"temp_min": 5.36,
				"temp_max": 5.36,
				"pressure": 976.28,
				"sea_level": 983.95,
				"grnd_level": 976.28,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 0.49,
				"deg": 79.5034
			},
			"rain": {
				"3h": 2.45
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-10 18:00:00"
		},
		{
			"dt": 1512939600,
			"main": {
				"temp": 5.31,
				"temp_min": 5.31,
				"temp_max": 5.31,
				"pressure": 974.84,
				"sea_level": 982.61,
				"grnd_level": 974.84,
				"humidity": 100,
				"temp_kf": 0
			},
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"clouds": {
				"all": 92
			},
			"wind": {
				"speed": 1.45,
				"deg": 348
			},
			"rain": {
				"3h": 2.02
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2017-12-10 21:00:00"
		}
	],
	"city": {
		"id": 2643743,
		"name": "London",
		"coord": {
			"lat": 51.5085,
			"lon": -0.1258
		},
		"country": "GB"
	}
};
