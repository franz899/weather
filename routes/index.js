const express = require('express');
const router = express.Router();
const { getForecast } = require('./openWeatherMap');

const forecastViewModel = require('../viewmodels/forecastViewModel');

router.get('/', async function (req, res, next) {
  try {
    const forecast = await getForecast();
    res.render('index', {
      title: `London's weather forecast`,
      forecast: forecastViewModel.forecast(forecast.data)
    });
  } catch (err) {
    next(err);
  }
});

module.exports = router;
