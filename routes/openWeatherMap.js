const axios = require('axios');

const { generateUrl } = require('../helpers/urlHelper');
const openWeatherMapApiKey = process.env.OPENWEATHERMAP || require('../config/openWeatherMap');
const baseUrl = 'https://api.openweathermap.org/data/2.5/forecast?';
const query = {
  units: 'metric',
  appid: openWeatherMapApiKey,
};

async function getForecast(city = 'London,gb') {
  const url = generateUrl(baseUrl, { ...query, q: city});
  return await axios.get(url);
}

module.exports = {
  getForecast,
};
