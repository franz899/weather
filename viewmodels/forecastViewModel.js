const {
  fromJSTimeStamp,
  getDateKey,
  getTimeKey
} = require('../helpers/dateTimeHelper');

function toCompass(degrees) {
  return ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N'][Math.round(degrees / 11.25 / 2)];
}

function eighth(day) {
  const {
    dt,
    main,
    weather,
    rain,
    snow,
    wind,
  } = day;
  const { temp, humidity, pressure, } = main;
  const { description } = weather[0];
  const { speed, deg } = wind;
  return {
    time: getTimeKey(fromJSTimeStamp(dt)), 
    temp,
    humidity,
    pressure,
    description, 
    wind: {
      speed, // meter/sec
      direction: toCompass(deg),
    },
    rain: rain && rain['3h'] || null,
    snow: snow && snow['3h'] || null,
  };
}

function groupDays(list) {
  const days = {};
  list.forEach(day => {
    const date = getDateKey(fromJSTimeStamp(day.dt));
    if (!days.hasOwnProperty(date)) {
      days[date] = [];
    }
    days[date].push(eighth(day));
  });
  return days;
}

function forecast(forecast) {
  const { city, list } = forecast;
  const { name, coord, country } = city;
  return {
    city: { name, coord, country, },
    days: groupDays(list),
  };
}

module.exports = {
  toCompass,
  forecast,
  groupDays,
  eighth,
};