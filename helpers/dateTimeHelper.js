const { DateTime } = require('luxon');

function fromJSTimeStamp(seconds) {
  return DateTime.fromMillis(seconds * 1000).toObject();
}

function getDateKey(datetime) {
  return DateTime.fromObject(datetime).toFormat('dd-MM-yyyy');
}

function getTimeKey(datetime) {
  return DateTime.fromObject(datetime).toFormat('HH:mm');
}


module.exports = { 
  fromJSTimeStamp,
  getDateKey,
  getTimeKey,
};
