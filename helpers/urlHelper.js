function generateUrl(url, query) {
  const keys = Object
    .keys(query)
    .map(k => `${k}=${query[k]}`)
    .join('&');
  return url + keys;
}

module.exports = { generateUrl };
