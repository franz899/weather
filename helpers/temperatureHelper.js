function convertFahrenheitToCelsius(t) {
  return (5/9) * (t - 32)
}

function convertKelvinToCelsius(k) {
  return k - 273.15;
}

module.exports = {
  convertFahrenheitToCelsius,
  convertKelvinToCelsius,
};