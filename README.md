# README #

Simple web page that shows London's weather forecast for the next 5 days

I focused on creating a decent server side structure and a readable table for the web.
The next steps are to improve the tests and the html, and add support for css grid is available.

Use case: I want to know what the weather is going to be in the morning or when on my phone.

The UI was chosen to enable someone to load the webpage in a second, take a look at the forecast without any distraction and get on with her life.

There are a few things that I want to add, but it takes time to find a nice balance.

# To run the website you need an open weather map apikey
Get one and create put it in `config/openWeatherMap.js'
```
module.exports = "<yourAPIkey>";
```

# set up instructions
`yarn` to install dependencies

## for production
`yarn start`

Reach the website at `localhost:3000`


## For development
`yarn dev` and `yarn jest-watch`


The website is hosted on Heroku at: `https://london-weather-forecast.herokuapp.com`



### Who do I talk to? ###

Valerio Francescangeli <valerio.francescangeli@gmail.com>
